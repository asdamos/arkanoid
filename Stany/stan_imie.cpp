
#include "stan_imie.hpp"
#include <sstream>
#include <iostream>

Stan_imie::Stan_imie(sf::RenderWindow * window, int wynik) : window(window), wynik(wynik)
{
    wyniki = new Ranking_plik("ranking.txt");
    font.loadFromFile("arial.ttf");
    wyniki->wczytaj();

    text_podaj_imie.setFont(font);
    text_imie.setFont(font);

    text_podaj_imie.setString("Podaj swoje imie");
    text_imie.setString("");

    text_podaj_imie.setCharacterSize(50);
    text_imie.setCharacterSize(50);

    text_podaj_imie.setPosition(0,0);
    text_imie.setPosition(0, 60);

    text_podaj_imie.setColor(sf::Color::White);
    text_imie.setColor(sf::Color::White);

    str_imie="";


}

Stan_imie::~Stan_imie()
{
    delete wyniki;
}

void Stan_imie::rysuj_obiekty()
{
    window->draw(text_podaj_imie);
    window->draw(text_imie);
}

std::string Stan_imie::zmien_stan(std::string komunikat)
{
    std::stringstream ss;
    ss<<komunikat;
    std::string kom;
    ss>>kom;
    if(komunikat == "ESCAPE")
    {
        return "WYJSCIE";
    }
    else if(komunikat == "ENTER") //wprowadzono imie
    {
        std::cout<<"wykrywam"<<std::endl;
        wyniki->dopisz_do_rankingu(str_imie, wynik);
        wyniki->zapisz();
        return "WYJSCIE";
    }
    else if(kom == "IMIE")
    {
        ss>>kom;
        str_imie+=kom;
        text_imie.setString(str_imie);
    }


    return "OK";
}
