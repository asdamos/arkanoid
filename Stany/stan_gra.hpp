#ifndef STAN_GRY_H
#define STAN_GRY_H

#include <SFML/Graphics.hpp>
#include "stan_abstrakcyjny.hpp"
#include "../Pilka_paletka/paletka.hpp"
#include "../Klocki/klocek.hpp"
#include "../Klocki/klocek_kwadratowy.hpp"
#include "../Klocki/klocek_prostokatny.hpp"




class Gra : public StanGry
{
private:
    sf::RenderWindow *window;
    sf::Font font;
    sf::Text text_punkty;
    int punkty;
    bool pilka_w_ruchu;
    int nr_poziomu;
    int liczba_poziomow;
    Paletka *paletka;
    Pilka *pilka;
    void ustaw_pilke_na_srodku_paletki();
    void wczytaj_plansze(int x);
    bool test_kolizji(sf::RectangleShape * rect1, sf::RectangleShape * rect2);
    std::vector<Klocek*> plansza;

public:
    Gra(sf::RenderWindow *window);
    ~Gra();
    std::string zmien_stan(std::string komunikat);
    void rysuj_obiekty();
    int zwroc_punkty();
};
#endif
