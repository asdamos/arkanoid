#include "stan_gra.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>


Gra::Gra(sf::RenderWindow *window) : window(window)
{
    font.loadFromFile("arial.ttf");
    punkty = 0;
    text_punkty.setFont(font);
    text_punkty.setCharacterSize(25);
    text_punkty.setPosition(0,0);
    text_punkty.setColor(sf::Color::White);

    pilka_w_ruchu=false;
    paletka = new Paletka(window);
    pilka = new Pilka(window);
    ustaw_pilke_na_srodku_paletki();


    std::fstream plik;
    plik.open("ustawienia.txt");
    if(plik.is_open())
    {
        plik>>liczba_poziomow;
    }
    plik.close();

    nr_poziomu=0;

    wczytaj_plansze(nr_poziomu);

}

void Gra::rysuj_obiekty()
{
    std::stringstream ss;
    ss << "Wynik: "<<punkty;
    std::string str = ss.str();

    text_punkty.setString(str.c_str());
    window->draw(text_punkty);
    paletka->rysuj();
    pilka->rysuj();

    for(unsigned int i=0; i<plansza.size(); i++)
    {
        plansza[i]->rysuj();
    }

}

std::string Gra::zmien_stan(std::string komunikat)
{
    if(komunikat == "ESCAPE")
    {
        return "WYJSCIE";
    }
    else if(komunikat == "ENTER")
    {
        pilka_w_ruchu=true;
    }
    else if(komunikat == "LEFT")
    {
        paletka->przesun(-10.0);
    }
    else if(komunikat == "RIGHT")
    {
        paletka->przesun(10.0);
    }

    if(pilka_w_ruchu)
    {
        if(plansza.empty())
        {
            nr_poziomu++;
            if(nr_poziomu==liczba_poziomow) //koniec gry
            {
                return "PODAJ_IMIE";
            }
            else
            {
                wczytaj_plansze(nr_poziomu);
                pilka_w_ruchu=false;
                pilka->ustaw_wektor(-2.0, -2.0);
            }
        }



        //Sprawdzanie kolizji
        bool zmiana_y = false;
        bool zmiana_x = false;

        //Gorna sciana
        if(pilka->test_kolizji(0.0, -10.0, 800.0, 0.0) == 2)
        {
            zmiana_y = true;
        }
        //Dolna sciana
        if (pilka->test_kolizji(0.0, 600.0, 800.0, 810.0) == 2)
        {
            //liczba_pilek--;
            pilka_w_ruchu=false;
            pilka->ustaw_wektor(-2.0, -2.0);

        }
        //Lewa sciana
        if (pilka->test_kolizji(-10.0, 0.0, 0.0, 600) == 1)
            zmiana_x= true;

        if (pilka->test_kolizji(800.0, 0.0, 810.0, 600) == 1)
            zmiana_x= true;


        int test_paletka = paletka ->test_kolizji(pilka);
        switch(test_paletka)
        {
        case 1:
            zmiana_x=true;
            break;
        case 2:
            zmiana_y=true;
            break;
        case 3:
            zmiana_x=true;
            zmiana_y=true;
            break;
        };

        for(int i=0; i<plansza.size(); i++)
        {
            int test_klocek = plansza[i]->test_kolizji(pilka);
            if(test_klocek>0)
            {
                punkty+=plansza[i]->zwroc_punkty();
                plansza.erase (plansza.begin()+i);
                i--; //jest jeden element mniej
            }
            switch(test_klocek)
            {
            case 1:
                zmiana_x=true;
                break;
            case 2:
                zmiana_y=true;
                break;
            case 3:
                zmiana_x=true;
                zmiana_y=true;
                break;

            }
        }

        if (zmiana_y)
            pilka->zmien_wektor(1, -1);
        if (zmiana_x)
            pilka->zmien_wektor(-1, 1);

        pilka->przesun();
    }
    else
    {
        ustaw_pilke_na_srodku_paletki();
    }

    return "OK";
}

void Gra::ustaw_pilke_na_srodku_paletki()
{
    double x_paletki = paletka->zwroc_x();
    double y_paletki = paletka->zwroc_y();
    //double wys_paletki = paletka->zwroc_wysokosc();
    double szer_paletki = paletka->zwroc_szerokosc();
    double promien_pilki = pilka->zwroc_promien();

    double y_pilki = y_paletki - 2*promien_pilki;
    double x_pilki = x_paletki + (szer_paletki/2) - promien_pilki;

    pilka->ustaw_polozenie(x_pilki, y_pilki);
}

Gra::~Gra()
{
    delete paletka;
    delete pilka;
}


void Gra::wczytaj_plansze(int x)
{
    std::stringstream ss;
    ss << "plansze\\" << x<<".txt";
    std::string str = ss.str();

    std::fstream plik;
    plik.open(str.c_str());
    if(plik.is_open())
    {
        int liczba_klockow;
        plik>>liczba_klockow;
        std::getline(plik, str);
        for(int i=0; i<liczba_klockow; i++)
        {
            std::getline(plik, str);
            std::stringstream ss2;
            ss2<<str;
            ss2>>str;
            if(str == "kwadrat")
            {
                double x,y,szerokosc, r, g, b,alpha, p;
                ss2>>x>>y>>szerokosc>>r>>g>>b>>alpha>>p;
                Klocek * k = new Klocek_kwadratowy(window, x, y, szerokosc, r, g, b, alpha, p);
                sf::RectangleShape * rect_k = k->zwroc_ksztalt();
                sf::RectangleShape * rect_2;
                bool ok=true;
                //Sprawdzenie czy klocek z zadnym nie koliduje
                for(unsigned int j = 0; j<plansza.size(); j++)
                {
                    rect_2 = plansza[j]->zwroc_ksztalt();

                    //Koliduja ze soba
                    if(test_kolizji(rect_k, rect_2))
                    {
                        ok=false;
                        break;
                    }
                }
                if(!ok)
                {
                    delete k;
                }
                else
                {
                    plansza.push_back(k);
                }

            }
            else if(str == "prostokat")
            {
                double x,y,szerokosc, wysokosc, r, g, b, alpha, p;
                ss2>>x>>y>>szerokosc>>wysokosc>>r>>g>>b>>alpha>>p;

                Klocek * k = new Klocek_prostokatny(window, x, y, szerokosc, wysokosc, r, g, b, alpha, p);
                sf::RectangleShape * rect_k = k->zwroc_ksztalt();
                sf::RectangleShape * rect_2;
                bool ok=true;
                //Sprawdzenie czy klocek z zadnym nie koliduje
                for(unsigned int j = 0; j<plansza.size(); j++)
                {
                    rect_2 = plansza[j]->zwroc_ksztalt();

                    //Koliduja ze soba
                    if(test_kolizji(rect_k, rect_2))
                    {
                        ok=false;
                        break;
                    }
                }
                if(!ok)
                {
                    delete k;
                }
                else
                {
                    plansza.push_back(k);
                }
            }
        }
    }
    plik.close();
}

bool Gra::test_kolizji(sf::RectangleShape * rect1, sf::RectangleShape * rect2)
{
    sf::FloatRect r1=rect1->getGlobalBounds();
    sf::FloatRect r2=rect2->getGlobalBounds();
    return r1.intersects (r2);
}

int Gra::zwroc_punkty()
{
    return punkty;
}
