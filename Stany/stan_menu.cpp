

#include "stan_menu.hpp"
#include <iostream>


Menu::Menu(sf::RenderWindow *window) : window(window)
{
    font.loadFromFile("arial.ttf");

    zaznaczony = 0;

    text_graj.setFont(font);
    text_ranking.setFont(font);

    text_graj.setString("Graj");
    text_ranking.setString("Ranking");

    //W pixelach
    text_graj.setCharacterSize(50);
    text_ranking.setCharacterSize(50);

    text_graj.setPosition(0,0);
    text_ranking.setPosition(0, 60);

    text_graj.setColor(sf::Color::White);
    text_ranking.setColor(sf::Color(255,255,255,100)); //Szary
}

std::string Menu::zmien_stan(std::string komunikat)
{
    sf::Color grey = sf::Color(255,255,255,100);
    if(komunikat == "ESCAPE")
    {
        return "WYJSCIE";
    }
    else if(komunikat == "ENTER")
    {
        if(zaznaczony == 0)
        {
            return "WLACZ_GRE";
        }
        else
        {
            return "WLACZ_RANKING";
        }
    }
    else if(komunikat == "UP")
    {
        if(zaznaczony == 0)
        {
            zaznaczony = 1;
            text_graj.setColor(grey);
            text_ranking.setColor(sf::Color::White);
        }
        else if(zaznaczony == 1)
        {
            zaznaczony = 0;
            text_graj.setColor(sf::Color::White);
            text_ranking.setColor(grey);
        }
    }
    else if(komunikat == "DOWN")
    {
        if(zaznaczony == 0)
        {
            zaznaczony = 1;
            text_graj.setColor(grey);
            text_ranking.setColor(sf::Color::White);
        }
        else if(zaznaczony == 1)
        {
            zaznaczony = 0;
            text_graj.setColor(sf::Color::White);
            text_ranking.setColor(grey);
        }
    }

    return "OK";
}

void Menu::rysuj_obiekty()
{
    window->draw(text_graj);
    window->draw(text_ranking);
}

