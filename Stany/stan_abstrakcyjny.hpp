#ifndef STAN_ABS_H
#define STAN_ABS_H
#include <SFML/Graphics.hpp>

//Abstrakcyjna klasa stanu gry
class StanGry
{
private:
    sf::RenderWindow *window;
public:
    StanGry() {};
    ~StanGry() {};
    virtual std::string zmien_stan(std::string komunikat) =0;
    virtual void rysuj_obiekty() =0;
};

#endif
