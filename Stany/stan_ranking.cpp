#include "stan_ranking.hpp"

#include <sstream>
#include <iostream>

Ranking::Ranking(sf::RenderWindow *window) : window(window)
{
    wyniki = new Ranking_plik("ranking.txt");
    font.loadFromFile("arial.ttf");
    wyniki->wczytaj();
    n=10;

    for(int i=0; i<n; i++)
    {

        sf::Text t;
        t.setFont(font);
        t.setCharacterSize(45);
        t.setPosition(0,i*(45+5));
        t.setColor(sf::Color::White);

        std::stringstream ss;
        ss<<wyniki->zwroc_imie(i)<<" "<<wyniki->zwroc_wynik(i);
        std::string str = ss.str();
        t.setString(str);
        text_kontener.push_back(t);
    }
}

Ranking::~Ranking()
{
    delete wyniki;
}

void Ranking::rysuj_obiekty()
{
    for(int i=0; i<n; i++)
    {
        window->draw(text_kontener[i]);
    }
}

std::string Ranking::zmien_stan(std::string komunikat)
{
    if(komunikat == "ESCAPE")
    {
        return "WYJSCIE";
    }

    return "OK";
}


