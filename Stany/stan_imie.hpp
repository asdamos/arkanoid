#ifndef STAN_IMIE_H
#define STAN_IMIE_H

#include <SFML/Graphics.hpp>
#include "../ranking_obsluga_pliku.hpp"
#include "stan_abstrakcyjny.hpp"


class Stan_imie : public StanGry
{
private:
    sf::RenderWindow *window;
    sf::Font font;
    int n;
    Ranking_plik *wyniki;
    sf::Text text_podaj_imie;
    sf::Text text_imie;
    int wynik;
    std::string str_imie;
public:
    Stan_imie(sf::RenderWindow *window, int wynik);
    ~Stan_imie();
    std::string zmien_stan(std::string komunikat);
    void rysuj_obiekty();
};


#endif
