#ifndef STAN_MENU_H
#define STAN_MENU_H

#include "stan_abstrakcyjny.hpp"


class Menu : public StanGry
{
private:
    sf::RenderWindow *window;
    sf::Font font;
    int zaznaczony;             //zaznaczony wybor
    sf::Text text_graj;
    sf::Text text_ranking;
public:
    ~Menu() {};
    Menu(sf::RenderWindow *window);
    std::string zmien_stan(std::string komunikat);
    void rysuj_obiekty();
};
#endif
