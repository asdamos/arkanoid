#ifndef STAN_RANKING_H
#define STAN_RANKING_H

#include <SFML/Graphics.hpp>
#include "../ranking_obsluga_pliku.hpp"
#include "stan_abstrakcyjny.hpp"

class Ranking : public StanGry
{
private:
    sf::RenderWindow *window;
    sf::Font font;
    int n;
    Ranking_plik *wyniki;
    std::vector<sf::Text> text_kontener;
public:
    Ranking(sf::RenderWindow *window);
    ~Ranking();
    std::string zmien_stan(std::string komunikat);
    void rysuj_obiekty();
};
#endif
