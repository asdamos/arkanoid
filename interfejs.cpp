#include "interfejs.hpp"
#include <iostream>
#include "enumy.hpp"

Interfejs::Interfejs()
{
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    window = new sf::RenderWindow(sf::VideoMode(800, 600), "Blocks",sf::Style::Default, settings);
    window->setFramerateLimit(120);
    stan = new Menu(window);
}

void Interfejs::graj()
{
    while (window->isOpen())
    {
        std::string komunikat="CZAS";
        std::string wynik;
        std::string str;
        sf::String text;
        sf::Event event;
        while (window->pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::Closed:
                window->close();
                break;

            // key pressed
            case sf::Event::KeyPressed:
                if (event.key.code == sf::Keyboard::Up)
                {
                    std::cout<<"UP"<<std::endl;
                    komunikat="UP";
                }
                else if(event.key.code == sf::Keyboard::Down)
                {
                    std::cout<<"DOWN"<<std::endl;
                    komunikat="DOWN";
                }
                else if(event.key.code == sf::Keyboard::Left)
                {
                    std::cout<<"LEFT"<<std::endl;
                    komunikat="LEFT";
                }
                else if(event.key.code == sf::Keyboard::Right)
                {
                    std::cout<<"RIGHT"<<std::endl;
                    komunikat="RIGHT";
                }
                else if(event.key.code == sf::Keyboard::Return)
                {
                    komunikat="ENTER";
                }
                else if(event.key.code == sf::Keyboard::Escape)
                {
                    komunikat="ESCAPE";
                }
                break;

            case sf::Event::TextEntered:
                if(nr_stanu == IMIE)
                {
                    char c = static_cast<char>(event.text.unicode);
                    //if (event.text.unicode < 128)
                    {
                        if((c == '\r') || (c == '\n'))
                        {
                            std::cout<<"enter"<<std::endl;
                            komunikat="ENTER";
                        }
                        else
                        {
                            std::cout<<"text "<<c<<std::endl;
                            str += c;
                            text=str;
                            komunikat="IMIE " + text;
                        }
                    }
                }
                break;


            default:
                break;
            }
        }


        wynik = stan->zmien_stan(komunikat);
        if(wynik == "WYJSCIE")
        {
            switch (nr_stanu)
            {
            case MENU:
                window->close();
                break;

            case IMIE:
                delete stan;
                nr_stanu=RANKING;
                stan = new Ranking(window);
                break;

            case RANKING:
                delete stan;
                nr_stanu=MENU;
                stan = new Menu(window);
                break;

            case GRA:
                delete stan;
                nr_stanu=MENU;
                stan = new Menu(window);
                break;

            default:
                std::cout<<"cuda sie zdarzajo"<<std::endl;
                break;
            }

        }
        else if(wynik == "WLACZ_GRE")
        {
            delete stan;
            nr_stanu=GRA;
            stan = new Gra(window);
        }
        else if(wynik == "WLACZ_RANKING")
        {
            delete stan;
            nr_stanu=RANKING;
            stan = new Ranking(window);
        }
        else if(wynik == "PODAJ_IMIE")
        {
            Gra *tmp;
            tmp = (Gra*) stan;
            int wynik = tmp->zwroc_punkty();
            delete stan;
            nr_stanu = IMIE;
            stan = new Stan_imie(window, wynik);
        }

        window->clear();
        stan->rysuj_obiekty();
        window->display();

    }
}
