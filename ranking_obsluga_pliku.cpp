#include "ranking_obsluga_pliku.hpp"
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>


Ranking_plik::Ranking_plik(std::string nazwa_pliku) :nazwa_pliku(nazwa_pliku)
{
    n = 10;
    for(int i=0; i<n; i++)
    {
        wyniki.push_back(0);
        imiona.push_back("-");

    }
    suma_kontrolna = policz_sume_kontrolna();
}

int Ranking_plik::policz_sume_kontrolna()
{
    int suma = 0;
    for(int i=0; i<n; i++)
    {
        suma+=wyniki[i];
        suma+=imiona[i].length();
    }
    return suma;
}

void Ranking_plik::dopisz_do_rankingu(std::string imie, int wynik)
{
    for(int i=0; i<n; i++)
    {
        if(wyniki[i]<wynik)
        {
            for(int j=n-2; j>i; j--)
            {
                wyniki[j]=wyniki[j-1];
                imiona[j]=imiona[j-1];
            }
            imiona[i]=imie;
            wyniki[i]=wynik;
            break;
        }
    }
}

void Ranking_plik::wczytaj()
{
    std::string str_wejscie;
    std::string imie;
    int wynik;
    std::ifstream plik;
    plik.open(nazwa_pliku);
    int i=0;
    if(plik.is_open())
    {
        if(std::getline(plik,str_wejscie)) //ma sume kontrolna
        {
            suma_kontrolna=atoi(str_wejscie.c_str());

            while(std::getline(plik, str_wejscie) && i<n)  //wczytywanie wynikow
            {
                std::stringstream sstr;
                sstr.str(str_wejscie);
                sstr>>imie;
                sstr>>wynik;
                dopisz_do_rankingu(imie,wynik);
                i++;
            }
        }
    }

    plik.close();
}
void Ranking_plik::zapisz()
{
    std::ofstream plik;
    plik.open(nazwa_pliku);
    int suma=policz_sume_kontrolna();
    plik<<suma<<std::endl;
    for(int i=0; i<n; i++)
    {
        plik<<imiona[i]<<" "<<wyniki[i]<<std::endl;
    }
    plik.close();
}

int Ranking_plik::zwroc_wynik(int i)
{
    return wyniki[i];
}

std::string Ranking_plik::zwroc_imie(int i)
{
    return imiona[i];
}


