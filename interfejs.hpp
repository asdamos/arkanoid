#ifndef INTERFEJS_H
#define INTERFEJS_H

#include <SFML/Graphics.hpp>
#include "Stany/stan_abstrakcyjny.hpp"
#include "Stany/stan_gra.hpp"
#include "Stany/stan_menu.hpp"
#include "Stany/stan_ranking.hpp"
#include "Stany/stan_imie.hpp"

class Interfejs
{
private:
    sf::RenderWindow *window;
    int nr_stanu = 0;
    StanGry * stan;

public:
    Interfejs();



    void graj();
};
#endif
