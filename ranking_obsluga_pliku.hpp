#ifndef RANKING_OBSLUGA_H
#define RANKING_OBSLUGA_H

#include <string>
#include <vector>
#include <fstream>


class Ranking_plik
{
private:
    std::string nazwa_pliku;
    int suma_kontrolna;
    int n; //liczba wynikow
    std::vector<int> wyniki;
    std::vector<std::string> imiona;
    int policz_sume_kontrolna();
public:
    Ranking_plik(std::string nazwa_pliku);
    void dopisz_do_rankingu(std::string imie, int wynik);
    int zwroc_wynik(int i);
    std::string zwroc_imie(int i);
    void wczytaj();
    void zapisz();
};
#endif
