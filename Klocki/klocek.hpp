#ifndef KLOCEK_H
#define KLOCEK_H

#include <SFML/Graphics.hpp>
#include "../Pilka_paletka/pilka.hpp"

//Abstrakcyjna klasa klocka
class Klocek
{
private:
    sf::RenderWindow *window;
public:
    Klocek() {};
    virtual void rysuj() =0;
    virtual sf::RectangleShape * zwroc_ksztalt() =0;
    virtual int test_kolizji(Pilka *pilka) =0;
    virtual int zwroc_punkty() =0;
};
#endif
