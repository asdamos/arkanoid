
#ifndef KLOCEK_KWADRATOWY_H
#define KLOCEK_KWADRATOYW_H

#include "../Klocki/klocek.hpp"
#include "../Pilka_paletka/pilka.hpp"



class Klocek_kwadratowy : public Klocek
{
private:
    sf::RenderWindow *window;
    sf::RectangleShape *kwadrat;
    double x;
    double y;
    double szerokosc;
    int r;
    int g;
    int b;
    int alpha;
    int punkty;
public:
    Klocek_kwadratowy(sf::RenderWindow *window, double x, double y, double szerokosc,int r, int g, int b, int alpha, int punkty);
    void rysuj();
    sf::RectangleShape * zwroc_ksztalt();
    int test_kolizji(Pilka *pilka);
    int zwroc_punkty();
};
#endif
