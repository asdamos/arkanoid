
#include "klocek_kwadratowy.hpp"

Klocek_kwadratowy::Klocek_kwadratowy(sf::RenderWindow *window, double x, double y,
                                     double szerokosc, int r, int g, int b, int alpha, int punkty) :
                                        window(window), x(x), y(y), szerokosc(szerokosc),
                                        r(r), g(g), b(b), alpha(alpha), punkty(punkty)
{
    kwadrat = new sf::RectangleShape();
    kwadrat->setPosition(x, y);
    kwadrat->setSize(sf::Vector2f(szerokosc, szerokosc));
    kwadrat->setFillColor(sf::Color(r,g,b,alpha));
}

void Klocek_kwadratowy::rysuj()
{
    window->draw(*kwadrat);
}

sf::RectangleShape * Klocek_kwadratowy::zwroc_ksztalt()
{
    return kwadrat;
}

int Klocek_kwadratowy::test_kolizji(Pilka *pilka)
{
    double promien = pilka->zwroc_promien();
    double x_srodka = pilka->zwroc_x() + promien;
    double y_srodka = pilka->zwroc_y() + promien;

    int coll=0;

    double x1 = this->x;
    double y1 = this->y;
    double x2 = this->x + this->szerokosc;
    double y2 = this->y + this->szerokosc;
    double vx = pilka->zwroc_vx();
    double vy = pilka->zwroc_vy();

    if ((x_srodka + vx + promien >= x1
            && x_srodka + vx - promien <= x2)
            && (y_srodka + vy + promien >= y1
                && y_srodka + vy - promien <= y2))
    {
        if (x_srodka + promien < x1 &&
                x_srodka + vx + promien >= x1)
            coll|=1;
        if (x_srodka - promien > x2 &&
                x_srodka + vx - promien <= x2)
            coll|=1;
        if (y_srodka + promien < y1&&
                y_srodka + vy + promien >= y1)
            coll|=2;
        if (y_srodka - promien > y2 &&
                y_srodka + vy - promien <= y2)
            coll|=2;
    }
    return coll;
}

int Klocek_kwadratowy::zwroc_punkty()
{
    return punkty;
}
