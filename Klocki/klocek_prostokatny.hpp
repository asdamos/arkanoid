#ifndef KLOCEK_PROSTOKATNY_H
#define KLOCEK_PROSTOKATNY_H

#include "../Klocki/klocek.hpp"
#include "../Pilka_paletka/pilka.hpp"


class Klocek_prostokatny : public Klocek
{
private:
    sf::RenderWindow *window;
    sf::RectangleShape *prostokat;
    double x;
    double y;
    double szerokosc;
    double wysokosc;
    int r;
    int g;
    int b;
    int alpha;
    int punkty;
public:
    Klocek_prostokatny(sf::RenderWindow *window, double x, double y, double szerokosc, double wysokosc, int r, int g, int b, int alpha, int punkty);
    sf::RectangleShape * zwroc_ksztalt();
    void rysuj();
    int test_kolizji(Pilka *pilka);
    int zwroc_punkty();
};
#endif
