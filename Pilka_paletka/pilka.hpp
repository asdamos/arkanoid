
#ifndef PILKA_H
#define PILKA_H

#include <SFML/Graphics.hpp>


class Pilka
{
private:
    sf::RenderWindow *window;
    sf::CircleShape *okrag;
    double x;
    double y;
    double vx;
    double vy;
    double promien;
public:
    Pilka(sf::RenderWindow *window);
    void przesun();
    void rysuj();
    void ustaw_polozenie(double x, double y);
    double zwroc_x() {return x;};
    double zwroc_y() {return y;};
    double zwroc_vx() {return vx;};
    double zwroc_vy() {return vy;};
    double zwroc_promien() {return promien;};
    void zmien_wektor(double vx, double vy);
    void ustaw_wektor(double vx, double vy);
    int test_kolizji(double x1, double y1, double x2, double y2);
};
#endif
