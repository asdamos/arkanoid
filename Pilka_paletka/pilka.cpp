
#include "pilka.hpp"


Pilka::Pilka(sf::RenderWindow *window) : window(window)
{
    ustaw_wektor(-2.0, -2.0);
    promien = 10.0;
    okrag = new sf::CircleShape(promien);
}

void Pilka::rysuj()
{
    window->draw(*okrag);
}

void Pilka::ustaw_polozenie(double x, double y)
{
    this->x=x;
    this->y=y;
    okrag->setPosition(x, y);
}

void Pilka::przesun()
{
    x+=vx;
    y+=vy;
    okrag->setPosition(x, y);
}

void Pilka::zmien_wektor(double vx, double vy)
{
    this->vx *= vx;
    this->vy *= vy;
}

void Pilka::ustaw_wektor(double vx, double vy)
{
    this->vx = vx;
    this->vy = vy;
}

int Pilka::test_kolizji(double x1, double y1, double x2, double y2)
{
    double x_srodka = x + promien;
    double y_srodka = y + promien;

    int coll=0;
    if ((x_srodka + vx + promien >= x1
            && x_srodka + vx - promien <= x2)
            && (y_srodka + vy + promien >= y1
                && y_srodka + vy - promien <= y2))
    {
        if (x_srodka + promien < x1 &&
                x_srodka + vx + promien >= x1)
            coll=1;
        if (x_srodka - promien > x2 &&
                x_srodka + vx - promien <= x2)
            coll=1;
        if (y_srodka + promien < y1&&
                y_srodka + vy + promien >= y1)
            coll=2;
        if (y_srodka - promien > y2 &&
                y_srodka + vy - promien <= y2)
            coll=2;
    }
    return coll;
}
