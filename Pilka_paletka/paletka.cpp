#include "paletka.hpp"



Paletka::Paletka(sf::RenderWindow *window) : window(window)
{
    wysokosc = 10.0;
    szerokosc = 100.0;
    rozmiar_okna = window->getSize();
    y = rozmiar_okna.y - wysokosc;
    x = (rozmiar_okna.x - szerokosc) / 2;
    prostokat = new sf::RectangleShape;
    prostokat->setPosition(x, y);
    prostokat->setSize(sf::Vector2f(szerokosc, wysokosc));
}

int Paletka::przesun(double wart)
{
    if(wart + x + szerokosc > rozmiar_okna.x)
    {
        x = rozmiar_okna.x - szerokosc;
    }
    else if(wart + x < 0)
    {
        x = 0;
    }
    else
    {
        x = wart + x;
    }
    prostokat->setPosition(x, y);

    return 0;
}

void Paletka::rysuj()
{
    window->draw(*prostokat);
}


int Paletka::test_kolizji(Pilka *pilka)
{
    double promien = pilka->zwroc_promien();
    double x_srodka = pilka->zwroc_x() + promien;
    double y_srodka = pilka->zwroc_y() + promien;

    int coll=0;

    double x1 = this->x;
    double y1 = this->y;
    double x2 = this->x + this->szerokosc;
    double y2 = this->y + this->wysokosc;
    double vx = pilka->zwroc_vx();
    double vy = pilka->zwroc_vy();

    if ((x_srodka + vx + promien >= x1
            && x_srodka + vx - promien <= x2)
            && (y_srodka + vy + promien >= y1
                && y_srodka + vy - promien <= y2))
    {
        if (x_srodka + promien < x1 &&
                x_srodka + vx + promien >= x1)
            coll|=1;
        if (x_srodka - promien > x2 &&
                x_srodka + vx - promien <= x2)
            coll|=1;
        if (y_srodka + promien < y1&&
                y_srodka + vy + promien >= y1)
            coll|=2;
        if (y_srodka - promien > y2 &&
                y_srodka + vy - promien <= y2)
            coll|=2;
    }
    return coll;
}

