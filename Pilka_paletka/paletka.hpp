#ifndef PALETKA_H
#define PALETKA_H

#include <SFML/Graphics.hpp>
#include "pilka.hpp"

class Paletka
{
private:
    sf::Vector2<unsigned int> rozmiar_okna;
    sf::RenderWindow *window;
    sf::RectangleShape *prostokat;
    double x;
    double y;
    double wysokosc;
    double szerokosc;
public:
    Paletka(sf::RenderWindow *window);
    int przesun(double wart);
    void rysuj();
    double zwroc_x() {return x;};
    double zwroc_y() {return y;};
    double zwroc_wysokosc() {return wysokosc;};
    double zwroc_szerokosc() {return szerokosc;};
    int test_kolizji(Pilka *pilka);
};
#endif
